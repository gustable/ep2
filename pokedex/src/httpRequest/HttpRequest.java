/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package httpRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author Gustavo Veloso
 */
public class HttpRequest {

    /**
     * Get the value of a pre-defined status from a HTTP-Request
     * @param nome using the pokemon's name as parameter
     * @param status 0 = speed
     *              1 = special-defense
     *              2 = special-attack
     *              3 = defense
     *              4 = attack
     *              5 = hp
     * @throws java.net.MalformedURLException
     * @throws java.net.ProtocolException
     * @throws org.json.JSONException
     */
    public static void getStats(String nome, int[] status, String[] info) throws MalformedURLException, ProtocolException, IOException, JSONException {
        HttpURLConnection connection = null;
	String inputLine;
        StringBuilder response = new StringBuilder();
        
	try {
            String targetURL = "https://pokeapi.co/api/v2/pokemon/" + nome + "/";
            URL url = new URL(targetURL);
            
            connection = (HttpURLConnection) url.openConnection();
            
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type","application/json");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.setUseCaches(false);
            connection.setDoOutput(true);
            
            //If the HTTPRequest is valid
            if (connection.getResponseCode() == 200) {
                
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                
                JSONObject objJSON = new JSONObject(response.toString());
                JSONArray stats = objJSON.getJSONArray("stats");
                
                for(int i = 0; i < 6; i++) {
                    JSONObject objStatus = stats.getJSONObject(i);
                    status[i] = objStatus.getInt("base_stat");
                }
                
                JSONObject objSpecies = objJSON.getJSONObject("species");
                info[0] = objSpecies.getString("name");
                
                int id = objJSON.getInt("id");
                info[1] = Integer.toString(id);
            }
            else {
                System.out.println("Can't get data");
            }
	} catch (IOException e) {
            e.printStackTrace();
	}
    }
}
