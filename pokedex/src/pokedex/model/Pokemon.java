/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.model;

import httpRequest.HttpRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import org.json.JSONException;

/**
 *
 * @author Gustavo Veloso
 */
public class Pokemon {
    public String apelido;
    public String name;
    public String id;
    public int speed;
    public int specialDefense;
    public int specialAttack;
    public int defense;
    public int attack;
    public int hp;
    
    /**
     * 
     * @param nameOrId both pokemon's id or its name can be used as search parameter
     * @throws ProtocolException
     * @throws IOException
     * @throws MalformedURLException
     * @throws JSONException 
     */
    public Pokemon(String nameOrId) throws ProtocolException, IOException, MalformedURLException, JSONException {
        setPokemonInfos(nameOrId);
    }
    
    public Pokemon() {
        this.name = null;
        this.id = null;
    }
    
    public void setPokemonApelido(String apelido) {
        this.apelido = apelido;
    }

    public void setPokemonInfos(String nameOrId) throws ProtocolException, IOException, MalformedURLException, JSONException {
        int[] stats = new int[6];
        String[] info = new String[2];
        
        HttpRequest.getStats(nameOrId, stats, info);
        
        this.speed = stats[0];
        this.specialDefense = stats[1];
        this.specialAttack = stats[2];
        this.defense = stats[3];
        this.attack = stats[4];
        this.hp = stats[5];
        this.name = info[0];
        this.id = info[1];
    }
}
