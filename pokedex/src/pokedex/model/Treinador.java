/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.model;
import java.io.BufferedReader;
import httpRequest.HttpRequest;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import org.json.JSONException;

/**
 *
 * @author Gustavo Veloso
 */

public class Treinador {
    public String nickname;
    public String name;
    
    public ArrayList<Pokemon> pokemons;
    
    /**
     * Polimorfismo necessário devido ao método que utilizei para criar um
     * ArrayList de treinadores em CardLayoutController
     * @param nickname
     * @param name 
     */
    public Treinador(String nickname, String name) {
        setNameAndNickname(nickname, name);
        this.pokemons = new ArrayList();
    }
    
    public Treinador() {
        this.nickname = null;
        this.name = null;
        this.pokemons = new ArrayList();
    }
    
    public void setNameAndNickname(String nickname, String name) {
        this.nickname = nickname;
        this.name = name;
    }

    public void addPokemon(String nameOrId, String apelido) throws ProtocolException, IOException, MalformedURLException, JSONException {
        Pokemon pokeTemp = new Pokemon(nameOrId);
        pokeTemp.setPokemonApelido(apelido);
        this.pokemons.add(pokeTemp);
    }
    
    public void deletePokemon(String nameOrId) throws ProtocolException, IOException, MalformedURLException, JSONException {
        try {
            Pokemon pokeTemp = new Pokemon(nameOrId);
            this.pokemons.remove(pokeTemp);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
