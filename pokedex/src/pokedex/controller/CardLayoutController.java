/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.controller;

import java.awt.CardLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.json.JSONException;
import pokedex.model.Pokemon;
import pokedex.model.Treinador;
import pokedex.view.Janela;
import pokedex.view.PokemonInfo;
import pokedex.view.SearchPokemon;
import pokedex.view.Signup;

/**
 *
 * @author Gustavo Veloso
 */
public class CardLayoutController {
    
    public CardLayoutController(Janela frame) throws IOException, ProtocolException, MalformedURLException, JSONException {
        CardLayout cl = new CardLayout();
        JPanel currentPanel = new JPanel();
        frame.createMenu(cl, currentPanel);
        
        currentPanel.setLayout(cl);
        currentPanel.add(Signup.createPanel(cl, currentPanel), "signup");

        String nameOrId = null;
        currentPanel.add(SearchPokemon.createPanel(cl, currentPanel, nameOrId), "searchPokemon");
        
        frame.add(currentPanel);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
        
        cl.show(currentPanel, "searchPokemon");
    }
}
