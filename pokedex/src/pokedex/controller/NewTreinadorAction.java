/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.controller;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;

/**
 *
 * @author Gustavo Veloso
 */
public class NewTreinadorAction implements ActionListener {
    public CardLayout cl;
    public JPanel currentPanel;
    
    public NewTreinadorAction(CardLayout cl, JPanel currentPanel) {
        this.cl = cl;
        this.currentPanel = currentPanel;
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        cl.show(currentPanel, "signup");
    }
}
