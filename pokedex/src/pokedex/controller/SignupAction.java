/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.controller;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import pokedex.model.Treinador;
import pokedex.view.SearchPokemon;

/**
 *
 * @author Gustavo Veloso
 */
public class SignupAction implements ActionListener {
    
    public CardLayout cl;
    public JPanel currentPanel;
    public String name;
    public String nickname;
    public Treinador treinadorTemp;
    public ArrayList<Treinador> treinadores = new ArrayList();

    public SignupAction(CardLayout cl, JPanel currentPanel, JTextField name, JTextField nickname) {
        this.cl = cl;
        this.currentPanel = currentPanel;
        this.name = name.getText().toString();
        this.nickname = nickname.getText().toString();
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        Treinador treinadorTemp = new Treinador(this.nickname, this.name);
        this.treinadores.add(treinadorTemp);
        
        cl.show(currentPanel, "searchPokemon");
    }
}
