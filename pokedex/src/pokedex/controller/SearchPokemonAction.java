/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.controller;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.json.JSONException;
import pokedex.model.Pokemon;
import pokedex.view.PokemonInfo;

/**
 *
 * @author Gustavo Veloso
 */
public class SearchPokemonAction implements ActionListener {
    public CardLayout cl;
    public JPanel currentPanel;
    public JTextField searchInput;
    public String nameOrId;

    public SearchPokemonAction(CardLayout cl, JPanel currentPanel, JTextField searchInput) throws IOException, ProtocolException, MalformedURLException, JSONException {
        this.cl = cl;
        this.currentPanel = currentPanel;
        this.searchInput = searchInput;
    }
    
    @Override
    public void actionPerformed(ActionEvent event) {
        this.nameOrId = searchInput.getText().toString().toLowerCase();
        try {
            currentPanel.add(PokemonInfo.createPanel(cl, currentPanel, nameOrId), "pokemon");
        } catch (IOException ex) {
            Logger.getLogger(SearchPokemonAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(SearchPokemonAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        cl.show(currentPanel, "pokemon");
    }
    
    public String getNameOrId() {
        return this.nameOrId;
    }
}
