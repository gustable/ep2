/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.controller;

import pokedex.model.Treinador;
import pokedex.model.Pokemon;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import javax.swing.JFrame;
import org.json.JSONException;
import pokedex.view.Janela;
import pokedex.view.SearchPokemon;
import pokedex.view.Signup;

/**
 *
 * @author Gustavo Veloso
 */
public class Pokedex {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException, ProtocolException, MalformedURLException, JSONException {
        Janela frame = new Janela("Pokédex");
        frame.setSize(640,480);
        CardLayoutController clController = new CardLayoutController(frame);
    }
    
}
