/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.json.JSONException;
import pokedex.controller.SearchPokemonAction;
import pokedex.model.Pokemon;
/**
 *
 * @author Gustavo Veloso
 */
public class SearchPokemon {
    public static JPanel createPanel(CardLayout cl, JPanel currentPanel, String nameOrId) throws IOException, ProtocolException, MalformedURLException, JSONException {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        
        JPanel titlePanel = new JPanel();
        titlePanel.setLayout(new FlowLayout());
        JLabel titleLabel = new JLabel("Procurar pokémon por nome ou ID"); 
        titlePanel.add(titleLabel);
        
        JPanel searchPanel = new JPanel();
        JTextField searchInput = new JTextField(50);
        SearchPokemonAction searchPokemonAction = new SearchPokemonAction(cl, currentPanel, searchInput);
        nameOrId = searchPokemonAction.getNameOrId();
        searchPanel.add(searchInput);
        
        JPanel searchButtonPanel = new JPanel();
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(searchPokemonAction);
        searchButtonPanel.add(searchButton);
        
        mainPanel.add(titlePanel, BorderLayout.NORTH);
        mainPanel.add(searchPanel, BorderLayout.CENTER);
        mainPanel.add(searchButtonPanel, BorderLayout.SOUTH);
        
        return mainPanel;
    }
}
