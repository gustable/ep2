/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.view;

import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import pokedex.controller.NewTreinadorAction;

/**
 *
 * @author Gustavo Velos
 */
public class Janela extends JFrame{
    public Janela(String titulo) {
        super(titulo);
    }
    
    public void createMenu(CardLayout cl, JPanel currentPanel) {
        
        NewTreinadorAction newTreinadorAction = new NewTreinadorAction(cl, currentPanel);
        
        JMenu treinador = new JMenu("Treinador");
        JMenuItem registroTreinador = new JMenuItem("Novo treinador");
        registroTreinador.addActionListener(newTreinadorAction);
        JMenuItem pesquisarTreinador = new JMenuItem("Pesquisar treinador");
        treinador.add(registroTreinador);
        treinador.add(pesquisarTreinador);
        
        JMenu ajuda = new JMenu("Ajuda");
        JMenuItem visitarAPI = new JMenuItem("Visitar site PokéAPI");
        JMenuItem reportarErro = new JMenuItem("Reportar erro");
        ajuda.add(visitarAPI);
        ajuda.add(reportarErro);
        
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(treinador);
        menuBar.add(ajuda);
        
    }
}
