/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.view;

import httpRequest.HttpRequest;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.json.JSONException;
import pokedex.controller.PokemonInfoAction;
import pokedex.model.Pokemon;

/**
 *
 * @author Gustavo Veloso
 */
public class PokemonInfo {
    
    public static JPanel createPanel(CardLayout cl, JPanel currentPanel, String nameOrId) throws ProtocolException, IOException, MalformedURLException, JSONException {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        
        Pokemon pokemon = new Pokemon(nameOrId);
        PokemonInfoAction pokemonInfoAction = new PokemonInfoAction(cl, currentPanel);
        
        JPanel titlePanel = new JPanel();
        titlePanel.setLayout(new FlowLayout());
        JLabel titleLabel = new JLabel(pokemon.name);
        titlePanel.add(titleLabel);
        
        JPanel statsPanel = new JPanel();
        statsPanel.setLayout(new FlowLayout());        
        statsPanel.add(new JLabel("Speed"));
        statsPanel.add(new JLabel(Integer.toString(pokemon.speed)));       
        statsPanel.add(new JLabel("Special Defense"));
        statsPanel.add(new JLabel(Integer.toString(pokemon.specialDefense)));       
        statsPanel.add(new JLabel("Special Attack"));
        statsPanel.add(new JLabel(Integer.toString(pokemon.specialAttack)));       
        statsPanel.add(new JLabel("Defense"));
        statsPanel.add(new JLabel(Integer.toString(pokemon.defense)));      
        statsPanel.add(new JLabel("Attack"));
        statsPanel.add(new JLabel(Integer.toString(pokemon.attack)));
        statsPanel.add(new JLabel("HP"));
        statsPanel.add(new JLabel(Integer.toString(pokemon.hp)));
        
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout());
        JButton backButton = new JButton("Pesquisar outro pokémon");
        backButton.addActionListener(pokemonInfoAction);
        buttonPanel.add(backButton);
        
        mainPanel.add(titlePanel, BorderLayout.NORTH);
        mainPanel.add(statsPanel, BorderLayout.CENTER);
        mainPanel.add(buttonPanel, BorderLayout.SOUTH);
        
        return mainPanel;
    }
}
