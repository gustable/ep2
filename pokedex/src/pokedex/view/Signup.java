/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokedex.view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import pokedex.controller.SignupAction;
import pokedex.model.Treinador;

/**
 *
 * @author Gustavo Veloso
 */
public class Signup{
    
    public static JPanel createPanel(CardLayout cl, JPanel currentPanel) {
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        
        JPanel titlePanel = new JPanel();
        titlePanel.setLayout(new FlowLayout());
        JLabel titleLabel = new JLabel("Cadastrar novo treinador"); 
        titlePanel.add(titleLabel);
        
        JPanel signupFormPanel = new JPanel();
        signupFormPanel.setLayout(new FlowLayout());
        signupFormPanel.add(new JLabel("Nome"));
        JTextField nameInput = new JTextField(50);
        signupFormPanel.add(nameInput);
        signupFormPanel.add(new JLabel("Nickname"));
        JTextField nicknameInput = new JTextField(50);
        signupFormPanel.add(nicknameInput);
        
        SignupAction signupAction = new SignupAction(cl, currentPanel, nameInput, nicknameInput);
        
        JPanel signupButtonPanel = new JPanel();
        signupButtonPanel.setLayout(new FlowLayout());
        JButton signupButton = new JButton("Cadastrar");
        signupButton.addActionListener(signupAction);
        signupButtonPanel.add(signupButton);
        
        mainPanel.add(titlePanel, BorderLayout.NORTH);
        mainPanel.add(signupFormPanel, BorderLayout.CENTER);
        mainPanel.add(signupButtonPanel, BorderLayout.SOUTH);
        
        return mainPanel;
    }
}
